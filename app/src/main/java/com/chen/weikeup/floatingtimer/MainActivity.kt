package com.chen.weikeup.floatingtimer

import android.app.AlertDialog
import android.app.AppOpsManager
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Binder
import android.os.Bundle
import android.provider.Settings
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity


class MainActivity : AppCompatActivity() {
    private val systemWindowManager by lazy { getSystemService(Context.WINDOW_SERVICE) as WindowManager }
    private var floatingView: FloatingView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (checkOp(this, AppOpsManager.OPSTR_SYSTEM_ALERT_WINDOW)) {
            showFloatingView()
        } else {
            AlertDialog.Builder(this)
                .setTitle("權限不足！")
                .setMessage("請先至「應用程式資訊」開啟「允許在其他應用程式上層顯示」並重新啟動應用程式，才能使用懸浮視窗。")
                .setPositiveButton("開啟設定") { dialog, _ ->
                    // 開啟應用程式設定
                    val intent = Intent(
                        Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                        Uri.parse("package:com.chen.weikeup.floatingtimer")
                    )
                    startActivity(intent)
                    dialog.cancel()
                }
                .setNegativeButton("確定") { dialog, _ ->
                    dialog.cancel()
                }
                .setOnCancelListener {
                    finish()
                }
                .show()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        floatingView?.run {
            systemWindowManager.removeViewImmediate(view)
        }
    }

    /**
     * 檢查應用程式權限
     * */
    private fun checkOp(context: Context, op: String): Boolean {
        val opManager = context.getSystemService(Context.APP_OPS_SERVICE) as AppOpsManager
        return opManager.checkOpNoThrow(
            op,
            Binder.getCallingUid(),
            context.packageName
        ) == AppOpsManager.MODE_ALLOWED
    }

    /**
     * 顯示懸浮視窗
     * */
    private fun showFloatingView() {
        if (floatingView == null) {
            floatingView = FloatingView(this)
        }

        floatingView?.run {
            systemWindowManager.addView(view, layoutParams)
        }
    }
}
