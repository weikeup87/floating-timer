package com.chen.weikeup.floatingtimer

import android.app.Activity
import android.content.Context
import android.graphics.Color
import android.graphics.PixelFormat
import android.os.CountDownTimer
import android.view.*
import kotlinx.android.synthetic.main.view_floating_timer.view.*
import java.util.*
import kotlin.concurrent.schedule

class FloatingView(context: Context) {
    private val lp: WindowManager.LayoutParams = WindowManager.LayoutParams().apply {
        type = WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY
        x = context.resources.displayMetrics.widthPixels - 250
        y = context.resources.displayMetrics.heightPixels / 2
        width = (250f * context.resources.displayMetrics.densityDpi / 420).toInt()
        height = (250f * context.resources.displayMetrics.densityDpi / 420).toInt()
        format = PixelFormat.RGBA_8888
        gravity = Gravity.TOP or Gravity.START
        flags =
            WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE or WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL or
                    WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN or WindowManager.LayoutParams.FLAG_LAYOUT_INSET_DECOR or
                    WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH
    }

    val layoutParams = lp

    val view: View =
        LayoutInflater.from(context).inflate(R.layout.view_floating_timer, null).apply {
            var startX = 0
            var startY = 0
            var curX = 0
            var curY = 0
            setOnTouchListener { _, event ->
                when (event.action) {
                    MotionEvent.ACTION_DOWN -> {
                        startX = event.rawX.toInt()
                        startY = event.rawY.toInt()
                        curX = lp.x
                        curY = lp.y
                    }
                    MotionEvent.ACTION_MOVE -> {
                        lp.x = event.rawX.toInt() - startX + curX
                        lp.y = event.rawY.toInt() - startY + curY
                        (context as Activity).windowManager.updateViewLayout(this, lp)
                    }
                }

                true
            }

            var timerSec = 30 // Default time second
            val changeSec = 1
            var isStart = false
            var timer: CountDownTimer? = null

            plusView.setOnClickListener {
                timerSec = if (timerSec + changeSec > 999) 999 else timerSec + changeSec
                timeView.text = timerSec.toString()
            }

            minusView.setOnClickListener {
                timerSec = if (timerSec - changeSec < 1) 1 else timerSec - changeSec
                timeView.text = timerSec.toString()
            }

            timeView.setOnClickListener {
                if (isStart) { // Stop & reset timer
                    timer?.cancel()
                    timeView.text = timerSec.toString()
                } else { // Start timer
                    timer = object : CountDownTimer(timerSec * 1000L, 100) {
                        override fun onFinish() {
                            timeView.text = "0"
                            var switchColor = false
                            var times = 0
                            val blinkTimes = 50
                            Timer().schedule(0, 100) {
                                timeView.setTextColor(if (switchColor) Color.WHITE else Color.BLACK)
                                switchColor = !switchColor
                                if (times++ >= blinkTimes) {
                                    timeView.text = timerSec.toString()
                                    timeView.setTextColor(Color.BLACK)
                                    cancel()
                                }
                            }
                            isStart = false
                        }

                        override fun onTick(millisUntilFinished: Long) {
                            timeView.text = (millisUntilFinished / 1000).toString()
                        }
                    }.start()
                }
                isStart = !isStart
            }
        }
}